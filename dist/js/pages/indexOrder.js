"use strict"
$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders";
    var gOrder = {
        orders: [],
        filterOrder: function (paramOrderObj) {
            let vResultArray = [];
            vResultArray = this.orders.filter(function (orderObj) {
                return (orderObj.trangThai.toUpperCase().includes(paramOrderObj.trangThai) || paramOrderObj.trangThai === "")
                    &&
                    (orderObj.loaiPizza == paramOrderObj.loaiPizza || paramOrderObj.loaiPizza === "");
            });
            console.log(vResultArray);
            return vResultArray;
        }
    }
    const gTABLE_COLS = ["orderId", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien",
        "hoTen", "soDienThoai", "trangThai", "chi tiet"];
    const gORDERID_COL = 0;
    const gKICH_CO_COL = 1;
    const gLOAI_PIZZA_COL = 2;
    const gID_LOAI_NUOC_UONG = 3;
    const gTHANH_TIEN_COL = 4;
    const gHO_VA_TEN = 5;
    const gSO_DIEN_THOAI = 6;
    const gTRANG_THAI_COL = 7;
    const gCHI_TIET_COL = 8
    var gTable = $('#table-user').DataTable({
        columns: [
            { data: gTABLE_COLS[gORDERID_COL] },
            { data: gTABLE_COLS[gKICH_CO_COL] },
            { data: gTABLE_COLS[gLOAI_PIZZA_COL] },
            { data: gTABLE_COLS[gID_LOAI_NUOC_UONG] },
            { data: gTABLE_COLS[gTHANH_TIEN_COL] },
            { data: gTABLE_COLS[gHO_VA_TEN] },
            { data: gTABLE_COLS[gSO_DIEN_THOAI] },
            { data: gTABLE_COLS[gTRANG_THAI_COL] },
            { data: gTABLE_COLS[gCHI_TIET_COL] }
        ],
        columnDefs: [
            {
                targets: gCHI_TIET_COL,
                defaultContent: `
                <button class = "btn btn-success details" >Chi tiết</button>
                `
            }
        ]
    });
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    onPageLoading();
    $('#table-user').on('click', '.details', function () {
        onDetailBtnClick(this);
    });
    $('#btn-filter').on('click', function () {
        onFilterBtnClick(this);
    });
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onDetailBtnClick(paramDetailBtn) {
        console.log("Detail btn is clicked");
        let vRowIndex = $(paramDetailBtn).closest("tr");
        let vRowData = gTable.row(vRowIndex).data();
        console.log(vRowData);
    }
    function onFilterBtnClick(paramFilterBtn) {
        console.log(`${$(paramFilterBtn).html()} is clicked`);
        let vObj = {
            trangThai: "",
            loaiPizza: "" 
        };
        getFilterData(vObj);
        let vResultArray = gOrder.filterOrder(vObj);
        loadDataToTable(vResultArray);
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    function onPageLoading() {
        console.log("Page is loaded");
        $.ajax({
            url: gBASE_URL,
            type: 'GET',
            dataType: 'json',
            success: function (res) {
                gOrder.orders = res;
                console.log(gOrder.orders);
                loadDataToTable(gOrder.orders);
            },
            error: function (err) {
                console.log(err);
            }
        });
        loadDataToSelectOrderStatus();
        loadDataToSelectPizzaType();
    }
    function loadDataToTable(paramUsersObj) {
        gTable.clear();
        gTable.rows.add(paramUsersObj);
        gTable.draw();
    }
    function loadDataToSelectOrderStatus() {
        let vStatusArray = ["open", "confirmed", "cancel"]
        $('<option/>', {
            value: "",
            text: "Chọn tất cả"
        }).appendTo($('#select-order-status'));
        for (let bI = 0; bI < vStatusArray.length; bI++) {
            $('<option/>', {
                value: vStatusArray[bI].toUpperCase(),
                text: vStatusArray[bI]
            }).appendTo($('#select-order-status'));
        }
    }
    function loadDataToSelectPizzaType() {
        let vPizzaTypeArray = ["hawaii", "bacon", "seafood"];
        $('<option/>', {
            value: "",
            text: "Chọn tất cả"
        }).appendTo($('#select-pizza-type'));
        for (let bI = 0; bI < vPizzaTypeArray.length; bI++) {
            $('<option/>', {
                value: vPizzaTypeArray[bI].toUpperCase(),
                text: vPizzaTypeArray[bI]
            }).appendTo($('#select-pizza-type'));
        }
    }
    function getFilterData(paramFilterObj) {
        paramFilterObj.trangThai = $('#select-order-status').val();
        paramFilterObj.loaiPizza = $('#select-pizza-type').val();
    }
});

