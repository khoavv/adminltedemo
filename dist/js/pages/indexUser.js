/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
/* Mảng userObjects là mảng chứa dữ liệu user. Từng phần tử là object
* id: tự tăng lên 1. Ví dụ, hiện id lớn nhất là 6, thì khi thêm user mới, id sẽ là 7
*/
var gUser = {
  gUserObjects: [
    {
      id: 2,
      username: "quannv",
      firstname: "Ngo Van",
      lastname: "Quan",
      age: 18,
      email: "quan@gmail.com",
      roleId: 5
    },
    {
      id: 3,
      username: "longdh",
      firstname: "Do Hoang",
      lastname: "Long",
      age: 19,
      email: "long@gmail.com",
      roleId: 8
    },
    {
      id: 4,
      username: "hiendt",
      firstname: "Do Thi",
      lastname: "Hien",
      age: 29,
      email: "hien@gmail.com",
      roleId: 11
    },
    {
      id: 6,
      username: "lanht",
      firstname: "Ho Thi",
      lastname: "Lan",
      age: 27,
      email: "lan@gmail.com",
      roleId: 13
    }
  ],
  getUserObjs: function(paramUserId) {
    if (paramUserId === undefined) {
      return this.gUserObjects;
    } else {
      let vIndex = -1;
      let vIsFound = false;
      let vIterator = 0;
      while (!vIsFound && vIterator < this.gUserObjects.length) {
        if (paramUserId === this.gUserObjects[vIterator].id) {
          vIsFound = true;
          vIndex = vIterator;
        } else {
          vIterator++;
        }
      }
      return vIndex;
    }
  },
  isExitUsername: function (paramUsername) {
    let vIsFound = false;
    let vIterator = 0;
    if (gFormMode === gFORM_MODE_UPDATE) {
      while (!vIsFound && vIterator < this.gUserObjects.length) {
        if (this.gUserObjects[vIterator].username === paramUsername && gId != this.gUserObjects[vIterator].id) {
          vIsFound = true;
        } else {
          vIterator++;
        }
      }
    }
    return vIsFound;
  },
  isExitEmail: function (paramUserEmail) {
    let vIsFound = false;
    let vIterator = 0;
    if (gFormMode === gFORM_MODE_UPDATE) {
      while (!vIsFound && vIterator < this.gUserObjects.length) {
        if (this.gUserObjects[vIterator].email === paramUserEmail && gId != this.gUserObjects[vIterator].id) {
          vIsFound = true;
        } else {
          vIterator++;
        }
      }
    }
    return vIsFound;
  },
}


// TODO: Điền tiếp các phần tử tiếp theo của mảng Role (xem trong slide task specification)
var gRoleObjects = [
  {
    roleId: 5,
    roleName: "Admin"
  },
  {
    roleId: 8,
    roleName: "Manager"
  }, {
    roleId: 11,
    roleName: "Teacher"
  }, {
    roleId: 13,
    roleName: "Staff"
  },
];

var gSTT = 0;
const gFORM_MODE_NORMAL = "Normal";
const gFORM_MODE_INSERT = "Insert";
const gFORM_MODE_UPDATE = "Update";
const gFORM_MODE_DELETE = "Delete";
const gACTION_COLUMN = 7;
const gSTT_COLUMN = 0;
const gROLE_COLUMN = 6;
var gFormMode = gFORM_MODE_NORMAL;
var gUserTable = null;
var gId = null;
/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
  onPageLoading();
  $('#btn-filter').on("click", function () {
    onFilterBtnClick(this);
  });
  $('.cancel').on("click", function () {
    onBtnCancelClick(this);
  })
  $('#table-user').on("click", ".edit", function () {
    gFormMode = gFORM_MODE_UPDATE;
    $('#modal-user-info').modal('show');
    onBtnEditClick(this);
  });
  $('#table-user').on("click", ".remove", function () {
    gFormMode = gFORM_MODE_DELETE;
    $('#modal-delete').modal('show');
    onBtnDeleteClick(this);
  });
  $('#btn-save').on("click", function () {
    onBtnSaveClick(this);
  });
  $('#btn-confirm').on("click", function () {
    onConfirmDeleteBtnClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onFilterBtnClick(paramBtnFilter) {
  // read data
  console.log(`${$(paramBtnFilter).text()} Btn clicked`);
  let vRoleId = parseInt($('#select-role option:selected').val());
  // handle data
  let vUserObjects = gUserObjects.filter(e => e.roleId === vRoleId);
  // show data
  loadDataToTable(vUserObjects);
}
function onBtnCancelClick(paramCancelBtn) {
  $(paramCancelBtn).parents(".modal").modal('hide');
}
function onBtnEditClick(paramSaveBtn) {
  // get obj from row
  let vRowIndex = $(paramSaveBtn).closest("tr");
  let vUserObj = gUserTable.row(vRowIndex).data();
  gId = vUserObj.id;
  console.log("gId: " + gId);
  setDataToForm(vUserObj);
}
function onBtnSaveClick() {
  let vUserObj = {};
  getFormToData(vUserObj);
  console.log(vUserObj);
  if (validateData(vUserObj)) {
    let vUser = gUser.getUserObjs();
    vUser.splice(gUser.getUserObjs(gId), 1, vUserObj);
    resetForm();
    loadDataToTable(vUser);
    $('#modal-user-info').modal('hide');
  }
}
function onBtnDeleteClick(paramDeleteBtn) {
  console.log(`DeleBtn is clicked!!!`);
  let vRowIndex = $(paramDeleteBtn).closest("tr");
  let vUserObj = gUserTable.row(vRowIndex).data();
  gId = vUserObj.id;
  document.getElementById("text-username").innerHTML += " " + vUserObj.username;
  document.getElementById("text-firstname").innerHTML += " " + vUserObj.firstname;
  document.getElementById("text-lastname").innerHTML += " " + vUserObj.lastname;
  document.getElementById("text-age").innerHTML += " " + vUserObj.age;
  document.getElementById("text-email").innerHTML += " " + vUserObj.email;
  document.getElementById("text-role").innerHTML += " " + vUserObj.roleId;
}
function onConfirmDeleteBtnClick() {
  gUser.getUserObjs().splice(gUser.getUserObjs(gId), 1);
  resetForm();
  loadDataToTable(gUser.getUserObjs());
  $('#modal-delete').modal('hide');
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
function onPageLoading() {
  console.log("Page is loaded...");
  gUserTable = $("#table-user").DataTable({
    columns: [
      { data: "" }, //stt
      { data: "username" },
      { data: "firstname" },
      { data: "lastname" },
      { data: "email" },
      { data: "age" },
      { data: "roleId" },
      { data: "" } // action
    ],
    columnDefs: [
      {
        targets: gACTION_COLUMN,
        defaultContent: `
        <div class = "row">
        <button class="btn btn-success col-sm-6 edit"><i class="fa fa-edit"></i></button>
        <button class="btn btn-danger col-sm-6 remove"><i class="fa fa-trash"></i></button>
        </div>
        `
      },
      {
        targets: gSTT_COLUMN,
        render: () => {
          return ++gSTT;
        }
      },
      {
        targets: gROLE_COLUMN,
        render: (data) => { return renderRoleIdToRoleName(data) }
      }
    ]
  });
  loadDataToTable(gUser.getUserObjs());
  loadOptionToSelectRole();
}
function loadDataToTable(paramObj) {
  gUserTable.clear();
  gUserTable.rows.add(paramObj);
  gUserTable.draw();
}
function loadOptionToSelectRole() {
  gRoleObjects.forEach((obj) => {
    let vHtml = `<option value="${obj.roleId}">${obj.roleName}</option>`;
    $('#select-role').append(vHtml);
  })
}
function renderRoleIdToRoleName(paramRoleId) {
  for (let bI = 0; bI < gRoleObjects.length; bI++) {
    if (paramRoleId === gRoleObjects[bI].roleId) {
      return gRoleObjects[bI].roleName;
    }
  }
}
function setDataToForm(paramUserObj) {
  $('#inp-username').val(paramUserObj.username);
  $('#inp-firstname').val(paramUserObj.firstname);
  $('#inp-lastname').val(paramUserObj.lastname);
  $('#inp-age').val(paramUserObj.age);
  $('#inp-role').val(paramUserObj.roleId);
  $('#inp-email').val(paramUserObj.email);
}
function getFormToData(paramUserObj) {
  if (gFormMode === gFORM_MODE_UPDATE) {
    paramUserObj.id = gId;
    paramUserObj.username = $('#inp-username').val().trim();
    paramUserObj.firstname = $('#inp-firstname').val().trim();
    paramUserObj.lastname = $('#inp-lastname').val().trim();
    paramUserObj.age = $('#inp-age').val().trim();
    paramUserObj.email = $('#inp-email').val().trim();
    paramUserObj.roleId = parseInt($('#inp-role').val().trim());
  }
}
// function getId() {
//   return (gUserObjects === 0) ? 0 : gUserObjects[gUserObjects.length - 1].id + 1;
// }

function validateData(paramUserObj) {
  let vRegexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  let vListRoleId = gRoleObjects.map((e) => e.roleId);
  if (paramUserObj.firstname === "") {
    alert("first name khon hop le");
    return false;
  }
  if (paramUserObj.lastname === "") {
    alert("Last name khong hop le");
    return false;
  }
  if (!vRegexEmail.test(paramUserObj.email)) {
    alert("Email cua ban khong hop le");
    return false;
  }
  if (paramUserObj.age === "" || isNaN(parseInt(paramUserObj.age))
    || parseInt(paramUserObj.age) < 10 || parseInt(paramUserObj.age) > 180) {
    alert("Age cua ban khong hop le");
    return false;
  }
  if (paramUserObj.roleId === "" || isNaN(parseInt(paramUserObj.roleId)) || !vListRoleId.includes(parseInt(paramUserObj.roleId))) {
    alert("Role id khong hop le");
    return false;
  }
  if (gUser.isExitUsername(paramUserObj.username)) {
    alert("Username cua ban bi trung");
    return false;
  }
  if (gUser.isExitEmail(paramUserObj.email)) {
    alert("Email cua ban bi trung");
    return false;
  }
  return true;
}
// function isExitUsername(paramUsername) {
//   // debugger
//   let vIsFound = false;
//   let vIterator = 0;
//   if (gFormMode === gFORM_MODE_UPDATE) {
//     while (!vIsFound && vIterator < gUserObjects.length) {
//       if (gUserObjects[vIterator].username === paramUsername && gId != gUserObjects[vIterator].id) {
//         vIsFound = true;
//       } else {
//         vIterator++;
//       }
//     }
//   }
//   return vIsFound;
// }
// function isExitEmail(paramUserEmail) {
//   let vIsFound = false;
//   let vIterator = 0;
//   if (gFormMode === gFORM_MODE_UPDATE) {
//     while (!vIsFound && vIterator < gUserObjects.length) {
//       if (gUserObjects[vIterator].email === paramUserEmail && gId != gUserObjects[vIterator].id) {
//         vIsFound = true;
//       } else {
//         vIterator++;
//       }
//     }
//   }
//   return vIsFound;
// }
// function getIndexUserObjById(paramId) {
//   let vIndex = -1;
//   let vIsFound = false;
//   let vIterator = 0;
//   while (!vIsFound && vIterator < gUserObjects.length) {
//     if (paramId === gUserObjects[vIterator].id) {
//       vIsFound = true;
//       vIndex = vIterator;
//     } else {
//       vIterator++;
//     }
//   }
//   return vIndex;
// }

function resetForm() {
  gSTT = 0;
  $('#inp-username').val("");
  $('#inp-firstname').val("");
  $('#inp-lastname').val("");
  $('#inp-age').val("");
  $('#inp-role').val("");
  $('#inp-email').val("");
}
